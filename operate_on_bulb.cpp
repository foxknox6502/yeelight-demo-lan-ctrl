#include <QTcpSocket>
#include <QThread>
#include <QtDebug>
#include <QDataStream>
#include "operate_on_bulb.h"

operate_on_bulb::operate_on_bulb(QObject *parent) : QObject(parent)
{
}

void operate_on_bulb::toggle_bulb(QString bulb_ip, quint16 bulb_port)
{
    /* Do network stuff */
    tcp_socket = new QTcpSocket(this);
    tcp_socket->connectToHost(bulb_ip,bulb_port);

    if(!tcp_socket->waitForConnected(100))
    {
        qDebug() << tcp_socket->errorString();
    }
    else
    {
        tcp_socket->write("{\"id\":1,\"method\":\"toggle\",\"params\":[]}\r\n");
        while(tcp_socket->waitForBytesWritten());
        tcp_socket->close();
    }
}

void operate_on_bulb::start_cf(QString bulb_ip, quint16 bulb_port)
{
    tcp_socket = new QTcpSocket(this);
    tcp_socket->connectToHost(bulb_ip,bulb_port);

    if(!tcp_socket->waitForConnected(100))
    {
        qDebug() << tcp_socket->errorString();
    }
    else
    {
        tcp_socket->write("{\"id\":1,\"method\":\"start_cf\",\"params\":[0,2,\"5000,1,16711680,100,5000,1,16776960,100,5000,1,65280,100,5000,1,255,100,5000,1,16711935,100\"]}\r\n");
        while(tcp_socket->waitForBytesWritten());
        tcp_socket->close();
    }
}

void operate_on_bulb::set_ct(QString bulb_ip, quint16 bulb_port)
{
    tcp_socket = new QTcpSocket(this);
    tcp_socket->connectToHost(bulb_ip,bulb_port);


    if(!tcp_socket->waitForConnected(100))
    {
        qDebug() << tcp_socket->errorString();
    }
    else
    {
        tcp_socket->write("{\"id\":1,\"method\":\"set_ct_abx\",\"params\":[3500, \"smooth\", 500]}\r\n");
        while(tcp_socket->waitForBytesWritten());
        tcp_socket->close();
    }
}

void operate_on_bulb::set_bright(QString bulb_ip, quint16 bulb_port, int brightness)
{
    tcp_socket = new QTcpSocket(this);
    tcp_socket->connectToHost(bulb_ip,bulb_port);

   if(!tcp_socket->waitForConnected(100))
    {
        qDebug() << tcp_socket->errorString();
    }
    else
    {
       /* No fucking idea about how to do this */
       tcp_socket->write("{\"id\":1,\"method\":\"set_bright\",\"params\":[100, \"smooth\", 500]}\r\n");
       while(tcp_socket->waitForBytesWritten());
       tcp_socket->close();
    }
}
