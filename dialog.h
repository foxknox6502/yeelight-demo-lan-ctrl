#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = nullptr);
    ~Dialog();

private slots:
    void on_toggle_bulb_pushButton_clicked();
    void on_start_color_flow_pushButton_clicked();

    void on_set_ct_pushButton_clicked();

    void on_brightness_horizontalSlider_sliderMoved(int position);

private:
    Ui::Dialog *ui;
};

#endif // DIALOG_H
